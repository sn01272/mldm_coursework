
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Module Loading Starts..
:- use_module(aleph).
:- if(current_predicate(use_rendering/1)).
:- use_rendering(prolog).
:- endif.
:- aleph.
:-style_check(-discontiguous).
%Module Loading ends..
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- aleph_set(i,3).

:- modeh(1,recommended(+id)).
:- modeb(1,ground_service(+id,#val)).
:- modeb(1,value_for_money(+id,#val)).
:- determination(recommended/1,ground_service/2).
:- determination(recommended/1,value_for_money/2).




:-begin_bg. 

:-end_bg. 